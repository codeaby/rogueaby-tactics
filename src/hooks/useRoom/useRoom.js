import { useEffect, useState } from 'react';
import { Firestore, useAuth } from 'codeaby-framework';
import { ROOMS_COLLECTION } from '../../constants/constants';
import { useHistory } from 'react-router-dom';
import { postNewAction } from '../../services/RogueabyTacticsService';

const useRoom = (roomId) => {
  const { user } = useAuth();
  const history = useHistory();
  const [loaded, setLoaded] = useState(false);
  const [room, setRoom] = useState();
  const [currentTurnCharacter, setCurrentTurnCharacter] = useState(null);
  const [isMyTurn, setIsMyTurn] = useState(false);
  const [temporalBoard, setTemporalBoard] = useState(null);
  const [movement, setMovement] = useState(undefined);

  useEffect(() => {
    Firestore.readDocumentUpdates(
      ROOMS_COLLECTION,
      roomId,
      (doc) => {
        const newRoom = doc.data();
        const newCurrentTurnCharacter =
          newRoom.characters && newRoom.characters[newRoom.turns[newRoom.currentTurn]];
        setLoaded(true);
        setRoom(newRoom);
        setTemporalBoard(null);
        setCurrentTurnCharacter(newCurrentTurnCharacter);
        setIsMyTurn(newCurrentTurnCharacter && newCurrentTurnCharacter.player === user.uid);
      },
      (e) => {
        console.error(e);
        history.push('/');
      }
    );
  }, [roomId, history, user.uid]);

  const isCharacterMine = (character) => character.player === user.uid;

  const isCharacterTurn = (character) =>
    character && currentTurnCharacter && character.id === currentTurnCharacter.id;

  const moveCurrentCharacter = (destinationRow, destinationCol) => {
    setTemporalBoard(
      room.board.map((row, rowIndex) => ({
        row: row.row.map((col, colIndex) =>
          col && col === currentTurnCharacter.id
            ? null
            : rowIndex === destinationRow && colIndex === destinationCol
            ? currentTurnCharacter.id
            : col
        ),
      }))
    );
    setMovement({ row: destinationRow, col: destinationCol });
  };

  const resetBoard = () => {
    setTemporalBoard(null);
    setMovement(undefined);
  };

  const sendCharacterAction = async (action, target) => {
    await postNewAction(roomId, currentTurnCharacter.id, movement, action, target);
    setMovement(undefined);
  };

  return {
    loaded,
    ...room,
    player: user.uid,
    board: temporalBoard ? temporalBoard : room && room.board,
    isMyTurn,
    currentTurnCharacter,
    isCharacterMine,
    isCharacterTurn,
    moveCurrentCharacter,
    resetBoard,
    sendCharacterAction,
  };
};

export default useRoom;
