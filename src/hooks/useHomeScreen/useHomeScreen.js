import { useEffect, useState } from 'react';
import { TEAM_SIZE } from '../../constants/constants';
import { getMyTeam, postNewChoices, postNewTeam } from '../../services/RogueabyTacticsService';

const useHomeScreen = () => {
  const [loading, setLoading] = useState(true);
  const [team, setTeam] = useState(null);
  const [choices, setChoices] = useState(null);
  const [selection, setSelection] = useState([]);
  const [activeChoice, setActiveChoice] = useState(0);
  const [newTeam, setNewTeam] = useState(false);

  const getChoices = async () => {
    const { data } = await postNewChoices();
    setChoices(data.choices);
  };

  const getTeam = async () => {
    try {
      const { data } = await getMyTeam();
      if (data.team.characters.length < TEAM_SIZE) {
        await getChoices();
      } else {
        setTeam(data.team);
      }
    } catch (e) {
      if (e.response && e.response.status === 404) {
        setNewTeam(true);
        await getChoices();
      }
    }
    setLoading(false);
  };

  useEffect(() => {
    getTeam();
  }, []);

  const handleSelection = (selectionIndex, optionIndex) => {
    const newSelection = [...selection];
    newSelection[selectionIndex] = optionIndex;
    setSelection(newSelection);
    if (activeChoice + 1 === choices.length) {
      sendSelection(newSelection);
    } else {
      setActiveChoice(activeChoice + 1);
    }
  };

  const sendSelection = async (newSelection) => {
    setLoading(true);
    await postNewTeam(newSelection);
    setChoices(null);
    setSelection([]);
    getTeam();
  };

  return {
    loading,
    team,
    choices,
    newTeam,
    activeChoice,
    handleSelection,
  };
};

export default useHomeScreen;
