import { Favorite, MyLocation, Security, Visibility } from '@material-ui/icons';

export const ROOMS_COLLECTION = 'rogueaby_rooms';

export const ROOM_STATUS = {
  WAITING_FOR_PLAYER2: 'WAITING_FOR_PLAYER2',
  IN_PROGRESS: 'IN_PROGRESS',
  GAME_OVER: 'GAME_OVER',
};

export const ACTIONS = {
  TARGETS: {
    ENEMY: 'ENEMY',
    ALLY: 'ALLY',
    SELF: 'SELF',
  },
  EFFECTS: {
    DAMAGE: 'DAMAGE',
    HEAL: 'HEAL',
    DEFENSE: 'DEFENSE',
    HIDE: 'HIDE',
  },
  ICONS: {
    DAMAGE: <MyLocation />,
    HEAL: <Favorite />,
    DEFENSE: <Security />,
    HIDE: <Visibility />,
  },
};

export const TEAM_SIZE = 3;
