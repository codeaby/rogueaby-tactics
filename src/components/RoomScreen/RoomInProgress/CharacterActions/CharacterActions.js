import { Box, styled, Typography } from '@material-ui/core';
import { DirectionsRun, TimerOff } from '@material-ui/icons';
import React from 'react';
import { ACTIONS } from '../../../../constants/constants';
import CharacterActionButton from './CharacterActionButton/CharacterActionButton';

const Container = styled(Box)(({ theme }) => ({
  margin: theme.spacing(4, 0),
}));

const CharacterActions = ({
  isMyTurn,
  moving,
  moved,
  resetMove,
  setMoving,
  character,
  sendCharacterAction,
  currentAction,
  setCurrentAction,
}) => {
  return (
    <Container>
      {isMyTurn ? (
        <>
          <CharacterActionButton
            name="Moverse"
            resetName="Deshacer Movimiento"
            inProgress={moving}
            done={moved}
            action={() => {
              setMoving(true);
              setCurrentAction(null);
            }}
            reset={resetMove}
            icon={<DirectionsRun />}
          />

          <CharacterActionButton
            name={character.actions[0].name}
            inProgress={currentAction === character.actions[0]}
            action={() => {
              setMoving(false);
              setCurrentAction(character.actions[0]);
            }}
            icon={ACTIONS.ICONS[character.actions[0].effect]}
          />

          <CharacterActionButton
            name={character.actions[1].name}
            inProgress={currentAction === character.actions[1]}
            action={() => {
              setMoving(false);
              setCurrentAction(character.actions[1]);
            }}
            icon={ACTIONS.ICONS[character.actions[1].effect]}
          />

          <CharacterActionButton
            name="Terminar turno"
            action={sendCharacterAction}
            icon={<TimerOff />}
          />
        </>
      ) : (
        <Typography variant="body1" align="center">
          Esperando que tu oponente realice una acción...
        </Typography>
      )}
    </Container>
  );
};

export default CharacterActions;
