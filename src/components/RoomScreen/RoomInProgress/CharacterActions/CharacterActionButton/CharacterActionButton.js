import { Button, styled } from '@material-ui/core';
import React from 'react';

const StyledButton = styled(Button)(({ theme }) => ({
  marginBottom: theme.spacing(2),
  justifyContent: 'flex-start',
}));

const CharacterActionButton = ({ name, resetName, inProgress, done, action, reset, icon }) => {
  return (
    <StyledButton
      variant={inProgress ? 'outlined' : 'contained'}
      color="primary"
      fullWidth
      onClick={done ? reset : action}
      size="large"
      startIcon={icon}
    >
      {done ? resetName : name}
    </StyledButton>
  );
};

export default CharacterActionButton;
