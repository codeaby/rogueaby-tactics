import { Loading } from 'codeaby-framework';
import React, { useState } from 'react';
import { ACTIONS } from '../../../constants/constants';
import { jumpDistance, jumpDistanceDiagonally } from '../../../utils/distances';
import Board from './Board/Board';
import CharacterActions from './CharacterActions/CharacterActions';
import BattleLogs from './BattleLogs/BattleLogs';

const RoomInProgress = ({
  board,
  characters,
  logs,
  player,
  isCharacterMine,
  isCharacterTurn,
  isMyTurn,
  currentTurnCharacter,
  moveCurrentCharacter,
  resetBoard,
  sendCharacterAction,
}) => {
  const [loading, setLoading] = useState(false);
  const [moving, setMoving] = useState(false);
  const [moved, setMoved] = useState(false);

  const [currentAction, setCurrentAction] = useState(null);

  const distanceToCurrentCharacter = (row, col, distanceFunction = jumpDistance) => {
    const charRow = board.findIndex((row) =>
      row.row.find((col) => col === currentTurnCharacter.id)
    );
    const charCol = board[charRow].row.findIndex((col) => col === currentTurnCharacter.id);

    return distanceFunction(charRow, charCol, row, col);
  };

  const canMoveToCell = (row, col) =>
    !board[row].row[col] && distanceToCurrentCharacter(row, col) <= currentTurnCharacter.movement;

  const canTargetCell = (row, col) => {
    const cell = board[row].row[col];

    if (currentAction.target === ACTIONS.TARGETS.ENEMY) {
      const distance = distanceToCurrentCharacter(row, col, jumpDistanceDiagonally);
      const isValidDistance =
        distance >= currentAction.distance.min && distance <= currentAction.distance.max;
      return isValidDistance && (!cell || !isCharacterMine(characters[cell]));
    } else if (currentAction.target === ACTIONS.TARGETS.ALLY) {
      return cell && isCharacterMine(characters[cell]);
    } else {
      return (
        currentAction.target === ACTIONS.TARGETS.SELF &&
        cell &&
        characters[cell].id === currentTurnCharacter.id
      );
    }
  };

  const applyActionToTarget = async (character) => {
    setLoading(true);
    await sendCharacterAction(currentAction.name, character.id);
    setMoving(false);
    setMoved(false);
    setCurrentAction(null);
    setLoading(false);
  };

  const moveCharacter = (row, col) => {
    moveCurrentCharacter(row, col);
    setMoving(false);
    setMoved(true);
  };

  const resetMove = () => {
    setMoved(false);
    resetBoard();
  };

  const resetAndSend = async () => {
    await sendCharacterAction();
    setMoving(false);
    setMoved(false);
    setCurrentAction(null);
  };

  return (
    <div>
      <Board
        board={board}
        characters={characters}
        moving={moving}
        canMoveToCell={canMoveToCell}
        moveCharacter={moveCharacter}
        isCharacterMine={isCharacterMine}
        isCharacterTurn={isCharacterTurn}
        selecting={!!currentAction}
        canTargetCell={canTargetCell}
        applyActionToTarget={applyActionToTarget}
      />
      {loading ? (
        <Loading />
      ) : (
        <CharacterActions
          isMyTurn={isMyTurn}
          moving={moving}
          moved={moved}
          resetMove={resetMove}
          setMoving={setMoving}
          character={currentTurnCharacter}
          sendCharacterAction={resetAndSend}
          currentAction={currentAction}
          setCurrentAction={setCurrentAction}
        />
      )}

      <BattleLogs logs={logs} characters={characters} player={player} />
    </div>
  );
};

export default RoomInProgress;
