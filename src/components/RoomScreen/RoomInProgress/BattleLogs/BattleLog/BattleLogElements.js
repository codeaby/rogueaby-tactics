import { styled } from '@material-ui/core';

export const CharacterName = styled(({ mine, ...others }) => <span {...others} />)(
  ({ mine, theme }) => ({
    color: mine ? theme.palette.primary.light : theme.palette.secondary.main,
  })
);
