import { Typography } from '@material-ui/core';
import React from 'react';
import { ACTIONS } from '../../../../../constants/constants';
import { CharacterName } from './BattleLogElements';

const damageLog = (otherParams, characters, player) => {
  const target = characters[otherParams[1]];
  return (
    <>
      hizo {otherParams[2]} puntos de daño a{' '}
      <CharacterName mine={target.player === player}>{target.name}</CharacterName> con{' '}
      {otherParams[0]}.
    </>
  );
};

const healLog = (otherParams, characters, player) => {
  const target = characters[otherParams[1]];
  return (
    <>
      curo {otherParams[2]} puntos de vida a{' '}
      <CharacterName mine={target.player === player}>{target.name}</CharacterName> con{' '}
      {otherParams[0]}.
    </>
  );
};

const defenseLog = (otherParams) => `activo su defensa con ${otherParams[0]}.`;

const hideLog = (otherParams) => `se escondio con ${otherParams[0]}.`;

const moveLog = (otherParams) => `se movio hacia la posicion ${otherParams[0]},${otherParams[1]}.`;

const skipLog = () => `salteo su turno.`;

const ACTION_RESOLVERS = {
  [ACTIONS.EFFECTS.DAMAGE]: damageLog,
  [ACTIONS.EFFECTS.HEAL]: healLog,
  [ACTIONS.EFFECTS.DEFENSE]: defenseLog,
  [ACTIONS.EFFECTS.HIDE]: hideLog,
  MOVED: moveLog,
  SKIP: skipLog,
};

const BattleLog = ({ log, characters, player }) => {
  const [characterId, actionType, ...otherParams] = log.split(';');
  const character = characters[characterId];
  const action =
    ACTION_RESOLVERS[actionType] && ACTION_RESOLVERS[actionType](otherParams, characters, player);

  return (
    <Typography variant="body1">
      <CharacterName mine={character.player === player}>{character.name}</CharacterName> {action}
    </Typography>
  );
};

export default BattleLog;
