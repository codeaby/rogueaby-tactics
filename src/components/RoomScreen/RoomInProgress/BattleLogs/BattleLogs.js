import { Box, styled, Typography } from '@material-ui/core';
import React from 'react';
import BattleLog from './BattleLog/BattleLog';

const BattleLogsContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  maxHeight: 300,
  overflow: 'scroll',
});

const BattleLogs = ({ logs, characters, player }) => {
  const reversedLogs = [...logs].reverse();
  return (
    <Box>
      <Typography variant="h6">Logs de batalla</Typography>
      <BattleLogsContainer>
        {reversedLogs.map((log, index) => (
          <BattleLog key={`log-${index}`} log={log} characters={characters} player={player} />
        ))}
      </BattleLogsContainer>
    </Box>
  );
};

export default BattleLogs;
