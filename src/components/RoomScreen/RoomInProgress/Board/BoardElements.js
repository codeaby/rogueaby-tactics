import { Box, styled } from '@material-ui/core';

export const BoardMatrix = styled(Box)({
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
});

export const BoardCell = styled(({ canMove, canTargetCell, ...others }) => <Box {...others} />)(
  ({ canMove, canTargetCell }) => ({
    width: '16%',
    border: '1px solid',
    borderColor: canMove
      ? 'rgba(0,255,0,0.5)'
      : canTargetCell
      ? 'rgba(255,0,0,0.5)'
      : 'rgba(255,255,255,0.15)',
    paddingBottom: '16%',
    position: 'relative',
  })
);

export const BoardCellContent = styled(Box)({
  position: 'absolute',
  width: '100%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
});
