import React from 'react';
import { BoardCellContent } from '../BoardElements';
import { Character, CharacterName, LifeBar, LifeBarHeart } from './CharacterCellElements';

const CharacterCell = ({ character, mine, active }) => {
  return (
    <BoardCellContent>
      <LifeBar>
        {[...Array(character.hp).keys()].map((h, index) => {
          const defending = !!character.defense && character.hp - character.currentHP === index;

          return (
            <LifeBarHeart key={`heart-${index}`} defending={defending}>
              {character.currentHP >= character.hp - index ? 'favorite' : 'favorite_border'}
            </LifeBarHeart>
          );
        })}
      </LifeBar>
      <Character active={active} />
      <CharacterName mine={mine}>{character.name}</CharacterName>
    </BoardCellContent>
  );
};

export default CharacterCell;
