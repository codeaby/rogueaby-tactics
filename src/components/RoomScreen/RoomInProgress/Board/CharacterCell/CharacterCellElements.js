import { Box, Icon, styled, Typography } from '@material-ui/core';
import { Accessibility } from '@material-ui/icons';

export const LifeBar = styled(Box)({
  whiteSpace: 'nowrap',
});

export const LifeBarHeart = styled(({ defending, ...others }) => <Icon {...others} />)(
  ({ defending }) => ({
    fontSize: '0.75rem',
    color: defending ? 'white' : '#ff0000',
  })
);

export const Character = styled(({ active, ...others }) => <Accessibility {...others} />)(
  ({ active }) => ({
    color: active ? 'rgba(0,255,0,0.5)' : 'white',
    fontSize: '1rem',
  })
);

export const CharacterName = styled(({ mine, ...others }) => (
  <Typography variant="body2" {...others} />
))(({ mine, theme }) => ({
  color: mine ? theme.palette.primary.light : theme.palette.secondary.main,
}));
