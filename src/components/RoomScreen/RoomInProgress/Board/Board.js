import React from 'react';
import { BoardCell, BoardMatrix } from './BoardElements';
import CharacterCell from './CharacterCell/CharacterCell';

const Board = ({
  board,
  characters,
  moving,
  canMoveToCell,
  moveCharacter,
  isCharacterMine,
  isCharacterTurn,
  selecting,
  canTargetCell,
  applyActionToTarget,
}) => {
  return (
    <BoardMatrix>
      {board.map((row, rowIndex) =>
        row.row.map((col, colIndex) => {
          const character = col && characters[col];
          const canMove = moving && canMoveToCell(rowIndex, colIndex);
          const canTargetThisCell = selecting && canTargetCell(rowIndex, colIndex);
          const canTargetThisCharacter = canTargetThisCell && character;
          const action = canMove
            ? () => moveCharacter(rowIndex, colIndex)
            : canTargetThisCharacter
            ? () => applyActionToTarget(character)
            : () => {};

          return (
            <BoardCell
              key={`${rowIndex}-${colIndex}`}
              canMove={canMove}
              canTargetCell={canTargetThisCell}
              onClick={action}
            >
              {character && (
                <CharacterCell
                  character={character}
                  mine={isCharacterMine(character)}
                  active={isCharacterTurn(character)}
                />
              )}
            </BoardCell>
          );
        })
      )}
    </BoardMatrix>
  );
};

export default Board;
