import { Typography } from '@material-ui/core';
import React from 'react';

const WaitingForPlayer = () => {
  return (
    <div>
      <Typography variant="h6">Buscando un oponente...</Typography>
    </div>
  );
};

export default WaitingForPlayer;
