import { Button, Dialog, DialogContent, Typography } from '@material-ui/core';
import { Loading, MainLayout } from 'codeaby-framework';
import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { ROOM_STATUS } from '../../constants/constants';
import useRoom from '../../hooks/useRoom/useRoom';
import RoomInProgress from './RoomInProgress/RoomInProgress';
import WaitingForPlayer from './WaitingForPlayer/WaitingForPlayer';

const RoomScreen = () => {
  const { id: roomId } = useParams();
  const history = useHistory();

  const {
    loaded,
    status,
    winner,
    board,
    characters,
    logs,
    player,
    currentTurnCharacter,
    isCharacterMine,
    isCharacterTurn,
    isMyTurn,
    moveCurrentCharacter,
    resetBoard,
    sendCharacterAction,
  } = useRoom(roomId);

  if (!loaded)
    return (
      <MainLayout title="Rogueaby Tactics" backButtonLink="/">
        <Loading />
      </MainLayout>
    );

  return (
    <MainLayout title="Rogueaby Tactics" backButtonLink="/">
      {status === ROOM_STATUS.WAITING_FOR_PLAYER2 && <WaitingForPlayer />}
      {status === ROOM_STATUS.IN_PROGRESS && (
        <RoomInProgress
          board={board}
          characters={characters}
          logs={logs}
          player={player}
          isCharacterMine={isCharacterMine}
          isCharacterTurn={isCharacterTurn}
          isMyTurn={isMyTurn}
          currentTurnCharacter={currentTurnCharacter}
          moveCurrentCharacter={moveCurrentCharacter}
          resetBoard={resetBoard}
          sendCharacterAction={sendCharacterAction}
        />
      )}
      {status === ROOM_STATUS.GAME_OVER && (
        <Dialog open={true} fullWidth maxWidth="md">
          <DialogContent>
            <Typography variant="h6" align="center" style={{ margin: 16 }}>
              {player === winner ? 'Ganaste!' : 'Perdiste!'}
            </Typography>
            <Button fullWidth variant="contained" color="primary" onClick={() => history.push('/')}>
              Volver al inicio
            </Button>
          </DialogContent>
        </Dialog>
      )}
    </MainLayout>
  );
};

export default RoomScreen;
