import { Button } from '@material-ui/core';
import { Loading, MainLayout } from 'codeaby-framework';
import React from 'react';
import { useHistory } from 'react-router-dom';
import useHomeScreen from '../../hooks/useHomeScreen/useHomeScreen';
import { joinRoom } from '../../services/RogueabyTacticsService';
import Choices from './Choices/Choices';
import Team from './Team/Team';

const HomeScreen = () => {
  const { loading, team, choices, newTeam, activeChoice, handleSelection } = useHomeScreen();
  const history = useHistory();

  const joinNewRoom = async () => {
    const { data } = await joinRoom();

    history.push(`/rooms/${data.id}`);
  };

  if (loading)
    return (
      <MainLayout title="Rogueaby Tactics">
        <Loading />
      </MainLayout>
    );

  return (
    <MainLayout title="Rogueaby Tactics">
      {team && (
        <>
          <Team team={team} />
          <Button fullWidth variant="contained" color="primary" onClick={joinNewRoom}>
            Iniciar Batalla
          </Button>
        </>
      )}

      {choices && (
        <Choices
          title={newTeam ? 'Creando a tu equipo' : 'Actualizando tu equipo'}
          choices={choices}
          activeChoice={activeChoice}
          handleSelection={handleSelection}
        />
      )}
    </MainLayout>
  );
};

export default HomeScreen;
