import { CardContent, Typography } from '@material-ui/core';
import React from 'react';
import { StyledCard, StyledMedia } from './TeamCharacterElements';

const TeamCharacter = ({ name, hp, movement, description }) => {
  return (
    <StyledCard>
      <StyledMedia image="luchaby.gif" title="character image" />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {name}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          HP: {hp} <br />
          Movimiento: {movement} <br />
          {description}
        </Typography>
      </CardContent>
    </StyledCard>
  );
};

export default TeamCharacter;
