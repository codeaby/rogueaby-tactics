import { Card, CardMedia, styled } from '@material-ui/core';

export const StyledCard = styled(Card)(({ theme }) => ({
  display: 'flex',
  marginBottom: theme.spacing(2),
}));

export const StyledMedia = styled(CardMedia)(({ theme }) => ({
  width: 50,
  backgroundSize: 'contain',
  margin: theme.spacing(2),
}));
