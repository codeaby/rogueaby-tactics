import React from 'react';
import TeamCharacter from './TeamCharacter/TeamCharacter';

const Team = ({ team }) => {
  return team.characters.map((character) => (
    <TeamCharacter
      key={character.id}
      name={character.name}
      hp={character.hp}
      movement={character.movement}
      description={character.description}
    />
  ));
};

export default Team;
