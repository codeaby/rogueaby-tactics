import { Card, CardMedia, styled } from '@material-ui/core';

export const StyledCard = styled(Card)(({ theme }) => ({
  flex: 1,
  margin: theme.spacing(1),
}));

export const StyledMedia = styled(CardMedia)(({ theme }) => ({
  height: 100,
  backgroundSize: 'contain',
  margin: theme.spacing(2),
}));
