import { CardActionArea, CardContent, Typography } from '@material-ui/core';
import React from 'react';
import { StyledCard, StyledMedia } from './ChoiceButtonElements';

const ChoiceButton = ({ name, hp, movement, description, selectOption }) => {
  return (
    <StyledCard>
      <CardActionArea onClick={selectOption}>
        <StyledMedia image="luchaby.gif" title="character image" />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            HP: {hp} <br />
            Movimiento: {movement} <br />
            {description}
          </Typography>
        </CardContent>
      </CardActionArea>
    </StyledCard>
  );
};

export default ChoiceButton;
