import { Typography } from '@material-ui/core';
import React from 'react';
import ChoiceButton from './ChoiceButton/ChoiceButton';
import { ChoicesContainer } from './ChoicesElements';

const Choices = ({ title, choices, activeChoice, handleSelection }) => {
  return (
    <>
      <Typography variant="h6">{title}</Typography>
      <ChoicesContainer>
        {choices[activeChoice].options.map((option, index) => (
          <ChoiceButton
            key={option.id}
            name={option.name}
            hp={option.hp}
            movement={option.movement}
            description={option.description}
            selectOption={() => handleSelection(activeChoice, index)}
          />
        ))}
      </ChoicesContainer>
    </>
  );
};

export default Choices;
