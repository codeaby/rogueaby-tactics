import axios from 'axios';
import { Authentication } from 'codeaby-framework';

const BASE_URL = 'https://us-central1-codeaby-486b9.cloudfunctions.net';

const getAxios = async () =>
  axios.create({
    baseURL: `${BASE_URL}/rogueabytactics`,
    headers: await Authentication.getAuthorizationHeader(),
  });

export const getMyTeam = async () => (await getAxios()).get('/teams/me');

export const postNewChoices = async () => (await getAxios()).post('/choices');

export const postNewTeam = async (choices) => (await getAxios()).post(`/teams/me`, { choices });

export const joinRoom = async () => (await getAxios()).post('/rooms');

export const postNewAction = async (roomId, character, movement, action, target) =>
  (await getAxios()).post(`/rooms/${roomId}/actions`, { character, movement, action, target });
