export const jumpDistance = (aRow, aCol, bRow, bCol) =>
  Math.abs(bRow - aRow) + Math.abs(bCol - aCol);

export const jumpDistanceDiagonally = (aRow, aCol, bRow, bCol) =>
  Math.max(Math.abs(bRow - aRow), Math.abs(bCol - aCol));
