import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { useAuth, PrivateRoute, LoginScreen, Loading } from 'codeaby-framework';
import HomeScreen from '../components/HomeScreen/HomeScreen';
import RoomScreen from '../components/RoomScreen/RoomScreen';

const Routing = () => {
  const { loaded } = useAuth();

  if (!loaded) return <Loading fullScreen />;

  return (
    <BrowserRouter basename="/rogueaby-tactics">
      <Switch>
        <Route path="/login">
          <LoginScreen />
        </Route>
        <PrivateRoute path="/" exact>
          <HomeScreen />
        </PrivateRoute>
        <PrivateRoute path="/rooms/:id">
          <RoomScreen />
        </PrivateRoute>
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
  );
};

export default Routing;
